using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using LFGApp.Models;
using LFGApp.Repository;

namespace LFGApp.Pages.Games {
    public class CreateModel : PageModel {
        IAppRepository _appRepository;

        public CreateModel(IAppRepository appRepository) {
            _appRepository = appRepository;
        }

        public void OnGet() {
            PopulateConsoleOptions();
        }

        [BindProperty]
        public GameViewModel Game { get; set; }

        public IActionResult OnPost() {
            if (!ModelState.IsValid) {
                return Page();
            }

            var newGame = new Game {
                Title = Game.Title,
                ReleaseDate = Game.ReleaseDate
            };

            var gameID = _appRepository.CreateGame(newGame);

            foreach (var consoleSelection in Game.GameConsoleSelections.Where(x => x.Selected)) {
                var newGameConsole = new GameConsole {
                    ConsoleID = consoleSelection.ConsoleID
                };

                _appRepository.CreateGameConsole(newGameConsole);
            }

            return RedirectToPage("./Index");
        }

        private void PopulateConsoleOptions() {
            var allConsoles = _appRepository.GetAllConsoles();
            Game = new GameViewModel();
            Game.GameConsoleSelections = new List<GameConsoleSelection>();
            foreach (var console in allConsoles) {
                var newConsoleOption = new GameConsoleSelection {
                    Title = console.Title,
                    ConsoleID = console.ID
                };
                Game.GameConsoleSelections.Add(newConsoleOption);
            }
        }
    }
}