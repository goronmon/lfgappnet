﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LFGApp.Models;
using LFGApp.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace LFGApp.Pages.Games {
    public class IndexModel : PageModel {
        IAppRepository _appRepository;
        public IndexModel(IAppRepository appRepository) {
            _appRepository = appRepository;
        }

        public List<Game> Games { get; set; }
        public void OnGet() {
            Games = _appRepository.GetAllGames().ToList();
        }
    }
}