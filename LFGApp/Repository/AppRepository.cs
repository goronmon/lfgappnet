﻿using Dapper;
using Dapper.Contrib.Extensions;
using LFGApp.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace LFGApp.Repository {
    public class AppRepository: IAppRepository {
        private IConfiguration _configuration;
        private string _connectionString;
        public AppRepository(IConfiguration configuration) {
            _configuration = configuration;
            _connectionString = configuration.GetSection("ConnectionStrings").GetSection("AppDB").Value;
        }

        // Games
        public IEnumerable<Game> GetAllGames() {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var games = connection.Query<Game>("select * from Game");
                return games;
            }
        }
        public Game GetGame(long ID) {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var game = connection.Query<Game>("select * from Game where ID = @ID", new { ID }).FirstOrDefault();
                return game;
            }
        }
        public long CreateGame(Game game) {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var newGameID = connection.Insert(game);
                return newGameID;
            }
        }
        public void UpdateGame(Game game) {

        }

        // Consoles
        public IEnumerable<Models.Console> GetAllConsoles() {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var consoles = connection.Query<Models.Console>("select * from Console");
                return consoles;
            }
        }
        public Models.Console GetConsole(long ID) {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var console = connection.Query<Models.Console>("select * from Console where ID = @ID", new { ID }).FirstOrDefault();
                return console;
            }
        }
        public void CreateConsole(Models.Console console) {

        }
        public void UpdateConsole(Models.Console console) {

        }

        // GameConsoles
        public IEnumerable<GameConsole> GetByGameID(long gameID) {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var gameConsoles = connection.Query<GameConsole>("SELECT * FROM GameConsole WHERE GameID = " + gameID);
                return gameConsoles;
            }
        }

        public long CreateGameConsole(GameConsole gameConsole) {
            using (var connection = new NpgsqlConnection(_connectionString)) {
                var newGameID = connection.Insert(gameConsole);
                return newGameID;
            }
        }
    }
}
