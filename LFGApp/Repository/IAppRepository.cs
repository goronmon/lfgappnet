﻿using LFGApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LFGApp.Repository {
    public interface IAppRepository {
        // Games
        IEnumerable<Game> GetAllGames();
        Game GetGame(long ID);
        long CreateGame(Game game);
        void UpdateGame(Game game);

        // Consoles
        IEnumerable<Models.Console> GetAllConsoles();
        Models.Console GetConsole(long ID);
        void CreateConsole(Models.Console console);
        void UpdateConsole(Models.Console console);

        // GameConsoles
        IEnumerable<GameConsole> GetByGameID(long ID);
        long CreateGameConsole(GameConsole gameConsole);
    }
}
