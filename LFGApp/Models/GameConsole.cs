﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LFGApp.Models {
    public class GameConsole {
        public long ID { get; set; }
        public long GameID { get; set; }
        public int ConsoleID { get; set; }

        public Game Game { get; set; }
        public Console Console { get; set; }
    }
}
