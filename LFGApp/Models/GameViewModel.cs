﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LFGApp.Models {
    public class GameViewModel {
        public long GameID { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public List<GameConsoleSelection> GameConsoleSelections{ get; set; }
    }

    public class GameConsoleSelection {
        public int ConsoleID { get; set; }
        public string Title { get; set; }
        public bool Selected { get; set; }
    }
}
