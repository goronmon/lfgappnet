﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LFGApp.Models {
    public class Game {
        public long ID { get; set; }
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
    }
}
