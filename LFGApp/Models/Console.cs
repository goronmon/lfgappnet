﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LFGApp.Models {
    public class Console {
        public int ID { get; set; }
        public string Title { get; set; }
    }
}
